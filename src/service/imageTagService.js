import axios from "axios";
import store from "../store"

const imageTagService = {

    userStore: store.state.user,
    API_URL: import.meta.env.VITE_API_URL,

    addTagToImage(imageId, tagId) {
        return new Promise((resolve, reject) => {
            let formData = new FormData()
            formData.append('imageId', imageId)
            formData.append('tagId', tagId)
            axios.post(this.API_URL + '/imageTag', formData).then((response) => {
                if (response.status === 201) {
                    let imageTag = response.data
                    store.commit('user/updateImageTag',{imageId, imageTag})
                    resolve(response)
                }
            }).catch(err => {
                reject(err)
            })
        })
    },

    deleteImageTag(imageTagId, imageId) {
        return new Promise((resolve, reject) => {
            return axios.delete(this.API_URL + '/imageTag/'+imageTagId).then((response) => {
                if(response.status === 200) {
                    store.commit('user/deleteImageTag', {imageTagId, imageId})
                    resolve()
                }
            }).catch(err => {
                reject(err)
            })
        })
    }
}

export default imageTagService