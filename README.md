# gallery-frontend

This is the MyGallery frontend project for a simple gallery-application, where you can:

- upload images with name and tags
- delete images / update tags from images (delete tags / add tags)
- filter images by tags
- search for images by image name

To run this project you have to make sure you have the backend project for MyGallery, since this project does not have a dev server yet.

## Environment Variables
create '.env.local' from '.env.-example.local' and insert your backend api url.

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```
