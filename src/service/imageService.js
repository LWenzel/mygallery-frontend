import axios from "axios";
import store from "../store"

const imageService = {

    userStore: store.state.user,
    API_URL: import.meta.env.VITE_API_URL,

    uploadImage(image, imageData) {
        return new Promise((resolve, reject) => {
            let formData = new FormData()
            formData.append('image', image)
            formData.append('name', imageData.name)
            formData.append('tags', imageData.tags)
            formData.append('portrait', imageData.portrait)
            axios.post(this.API_URL + '/user/'+this.userStore.userId+'/image', formData).then((response) => {
                if (response.status === 201) {
                    store.commit('user/updateLastImageUpload', response.data)
                    resolve(response)
                }
            }).catch(err => {
                reject(err)
            })
        })
    },

    getAllImages() {
        return new Promise((resolve, reject) => {
            axios.get(this.API_URL +'/user/'+this.userStore.userId+'/image').then((response) => {
                if (response.status === 200) {
                    store.commit('user/updateImages', response.data)
                    resolve(response)
                }
            }).catch(err => {
                reject(err)
            })
        })
    },

    getFilteredImages(filter) {
        let tags = filter.map((tag) => {
            return String(tag.tagId)
        })
        return new Promise((resolve, reject) => {
            return axios(this.API_URL + '/image/user/'+this.userStore.userId+'/filter', {
                method: 'GET',
                params: {filter: String(tags)}
            }).then((response) => {
                if (response.status === 200) {
                    store.commit('user/updateImages', response.data)
                    resolve(response)
                }
            }).catch(err => {
                reject(err)
            })
        })
    },

    getSearchedImages(search) {
        return new Promise((resolve, reject) => {
            return axios(this.API_URL + '/image/user/'+this.userStore.userId+'/search', {
                method: 'GET',
                params: {search: search}
            }).then((response) => {
                if (response.status === 200) {
                    store.commit('user/updateImages', response.data)
                    resolve(response)
                }
            }).catch(err => {
                reject(err)
            })
        })
    },

    deleteImage(id) {
        return new Promise((resolve, reject) => {
            return axios.delete(this.API_URL + '/image/'+id).then((response) => {
                if(response.status === 200) {
                    store.commit('user/deleteImage', id)
                    resolve()
                }
            }).catch(err => {
                reject(err)
            })
        })
    }

}

export default imageService