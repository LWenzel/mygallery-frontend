import Vuex from 'vuex'
import user from './user'

const store = new Vuex.Store({
    strict: true,
    modules: {
        user
    }
})

export default store