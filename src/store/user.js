const getDefaultState = () => {
    return {
        //TODO: should be requested by login + session (Cookie)
        userId: 1,
        images: [],
        tags: []
    }
}

const state = getDefaultState()

export const mutations = {
    updateImages(state, fields) {
        state.images = fields.reverse()
    },
    updateTags(state, fields) {
        state.tags = fields.reverse()
    },
    updateLastImageUpload (state, field) {
        state.images.unshift(field)
    },
    deleteImage(state, id) {
        let index = state.images.findIndex(image => {
            return image.id === id
        })
        state.images.splice(index, 1)
    },
    updateImageTag(state, {imageId, imageTag}) {
        console.log(imageTag)
        let index = state.images.findIndex(image => {
            return image.id === imageId
        })
        state.images[index].tags.push(imageTag);
    },
    deleteImageTag(state, {imageTagId, imageId}) {
        let index = state.images.findIndex(image => {
            return image.id === imageId
        })
        let imageTagIndex = state.images[index].tags.findIndex(tags => {
            return tags.id === imageTagId
        })
        state.images[index].tags.splice(imageTagIndex, 1)
    },
    reset(state) {
      Object.assign(state, getDefaultState())
    }
}

export default {
    namespaced: true,
    state,
    mutations
}