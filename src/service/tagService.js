import axios from "axios";
import store from "../store"

const tagService = {

    userStore: store.state.user,
    API_URL: import.meta.env.VITE_API_URL,

    getAllTags() {
        return new Promise((resolve, reject) => {
            axios.get(this.API_URL + '/user/'+this.userStore.userId+'/tag').then((response) => {
                if (response.status === 200) {
                    store.commit('user/updateTags', response.data)
                    resolve(response)
                }
            }).catch(err => {
                reject(err)
            })
        })
    },

}

export default tagService